;;; parlib.el --- Library of missing sexp-based commands -*- lexical-binding: t -*-

;; Copyright (C) 2021, 2022 Walter Lewis

;; Author: Walter Lewis <wklew@mailbox.org>
;; Keywords: lisp
;; Url: https://git.sr.ht/~wklew/parlib
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Code:

(defun parlib-open-paren-p (pos)
  (= 4 (syntax-class (syntax-after pos))))

(defun parlib-close-paren-p (pos)
  (= 5 (syntax-class (syntax-after pos))))

(defun parlib-sexp-bounds (n)
  (let ((pos (point)))
    (cond
     ((parlib-open-paren-p pos)
      (if (< n 0)
          (cons (scan-sexps pos (1+  n))
                (scan-sexps pos 1))
        (cons pos
              (scan-sexps pos n))))
     ((parlib-close-paren-p (1- pos))
      (if (< n 0)
          (cons (scan-sexps pos n)
                pos)
        (cons (scan-sexps pos -1)
              (scan-sexps pos (1- n)))))
     (t
      (if (< n 0)
          (progn
            (skip-syntax-backward "^w_()")
            (skip-syntax-forward "w_")
            (cons (scan-sexps pos n)
                  (point)))
        (skip-syntax-forward "^w_()")
        (skip-syntax-backward "w_")
        (cons (point)
              (scan-sexps pos n)))))))

(provide 'parlib)

;;; parlib.el ends here
